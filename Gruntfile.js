/**
 * Created by tim on 16.12.14.
 */
module.exports = function(grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

     grunt.loadNpmTasks('grunt-gitinfo');

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
         concat: {
            controller: {
              src: ['js/controller/*.js'],
              dest: 'js/controller.js',
            },
            directive: {
              src: ['js/directives/*.js'],
              dest: 'js/directives.js',
            },
            module: {
              src: ['js/module/*.js'],
              dest: 'js/main_plugin.js',
            },
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %>\n <%= grunt.template.today("dd-mm-yyyy") %>\n Revision: <%= gitinfo.rev %> */\n'
            },
            dist: {
                files: {
                    '<%= concat.dist.dest %>': ['<%= concat.dist.dest %>']
                }
            }
        },
        cssmin: {
            add_banner: {
                options: {
                    banner: '/*! <%= pkg.name %>\n <%= grunt.template.today("dd-mm-yyyy") %>\n Revision: <%= gitinfo.rev %> */\n'
                },
                files: {
                    'css/main_css.css': ['css/main_css.css']
                }
            }
        },
        jshint: {
            files: ['Gruntfile.js', 'js/modules/*.js', 'js/directives/*.js', 'js/controller/*.js','components/**/js/*.js', 'js/main_js.js'],
            options: {
                // options here to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },
        compass: {
            dist: {
                options: {
                    config: 'config.rb'
                }
            }
        },
        watch: {
            compass: {
                files: [
                    'sass/*.scss',
	                'sass/modules/*.scss',
	                'components/**/sass/*.scss'
                ],
                tasks: ['gitinfo', 'compass:dist']
            },
            js: {
                files: ['<%= jshint.files %>'],
                tasks: ['gitinfo', 'concat']
            }
        }
    });

    grunt.registerTask('test', ['jshint']);
    grunt.registerTask('fullWatch', ['watch', 'cssmin', 'concat', 'uglify']);
    grunt.registerTask('lightWatch', ['compass:dist','concat', 'watch', 'uglify']);

    grunt.registerTask('default', ['compass:dist', 'cssmin', 'jshint', 'controller','concat', 'uglify', 'watch']);

};