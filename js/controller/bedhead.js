'use strict';

var myApp = angular.module('myApp', []);

myApp.controller("bedhead" ,function ($scope, $log, Data) {
    $scope.link = 'img/bett-image-full.png';

    $scope.headData = Data.get('head', 'black');

    $scope.head = ['green', 'grey', 'black'];

    $scope.changeData = function (type, key) {
        $scope.headData = Data.get(type, key);
    };
});

// you may add more controllers below