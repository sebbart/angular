"use strict";

/*
 * Services can be defined as : "value", "service", "factory", "provider", or "constant".
 *
 * For simplicity only example of "value" and "service" are shown here. 
 */

// EXAMPLE OF CORRECT DECLARATION OF SERVICE AS A VALUE
myApp.value('version', '0.1');


myApp.service("Data", function () {
    var data = {
        head: {
            green: {
                image: 'img/bett-image-full-green.png',
                color: 'green',
                title: 'Head-Classic',
                price: 1200
            },
            black: {
                image: 'img/bett-image-full-black.png',
                color: 'black',
                title: 'Head-Classic',
                price: 1400
            },
            grey: {
                image: 'img/bett-image-full.png',
                color: 'grey',
                title: 'Head-Classic',
                price: 1100
            },
            blue: {
                color: 'blue',
                title: 'Head-Classic',
                price: 900
            }
        }
    };

    return {
        get: function(type, key) {
            return data[type][key];
        }
    };
});
