'use strict';

/**
 * @ngdoc filter
 * @name leadsApp.filter:capitalize
 * @function
 * @description
 * # capitalize
 * Filter in the leadsApp.
 */
myApp
    .filter('capitalize', function () {
        return function (input) {
            if (input) {
                return input.charAt(0).toUpperCase() + input.substr(1).toLowerCase();
            }
        };
    });
